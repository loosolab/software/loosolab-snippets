define({
    'name' : 'Scanpy',
    'sub-menu' : [
        {
            'name' : 'Documentation',
            'external-link' : 'https://scanpy.readthedocs.io',
        },
        {
            'name': 'Add watermark',
            'type': 'code',
            'snippet': [
                        '%load_ext watermark',
                        '%watermark -nuvhm -p numpy,pandas,anndata,scanpy,bbknn,geosketch -a \'YOUR AUTHOR NAME\']'
            ]
        },
        {
            'name': 'Setup',
            'type': 'code',
            'snippet' : [
                'import numpy as np',
                'import pandas as pd',
                'import scanpy',
                'scanpy.settings.verbosity = 3',
                'scanpy.set_figure_params(color_map=\'viridis\', dpi_save=300)',
                'scanpy.logging.print_header()'
            ],
        },
        '---',
        {
            'name' : 'Load data',
            'sub-menu' : [
                {
                    'name' : 'Documentation',
                    'external_link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#reading'
                },
                '---',
                {
                    'name': 'Read from h5ad file (AnnData)',
                    'type': 'code',
                    'snippet': ['adata = scanpy.read_h5ad(filename=\'\')']
                },
                {
                    'name' : 'Read Matrix Market file (.mtx extension)',
                    'type' : 'code',
                    'snippet' : ['adata = scanpy.read_mtx(filename=\'\', dtype=\'float32\')']
                },
                {
                    'name' : 'Read 10x-Genomics-formatted hdf5 file',
                    'type' : 'code',
                    'snippet' : ['adata = scanpy.read_10x_h5(filename=\'\', genome=None, gex_only=True)']
                },
                {
                    'name' : 'Save Anndata object to h5df file',
                    'type' : 'code',
                    'snippet' : ['adata.write(filename=\'\', compression=\'gzip\')']
                }
            ]
        },
        {
            'name' : 'Preprocessing',
            'sub-menu' : [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#module-scanpy.pp'
                },
                '---',
                {
                    'name' : 'Calculate quality control metrics',
                    'type': 'code',
                    'snippet': [
                        '# Find mitochondrial features',
                        'is_mito = adata.var_names.str.startswith(\'mt\') | adata.var_names.str.startswith(\'MT\')',
                        'adata.var[\'is_mito\'] = is_mito',
                        '',
                        'scanpy.pp.calculate_qc_metrics(adata, qc_vars=[\'is_mito\'], inplace=True)'],
                },
                {
                    'name' : 'Filter cells',
                    'type': 'code',
                    'snippet': ['scanpy.pp.filter_cells(adata, min_counts = , min_genes = , max_counts = , max_genes = )']
                },
                {
                    'name' : 'Filter genes',
                    'type': 'code',
                    'snippet': ['scanpy.pp.filter_genes(adata, min_counts = , min_cells = , max_counts = , max_cells = )']
                },
                {
                    'name' : 'Normalize counts per cell',
                    'type' : 'code',
                    'snippet': ['# Normalize total count per cell to median counts over all cells', 
                                'scanpy.pp.normalize_total(adata, target_sum = None)',
                                '',
                                '# Stabilize variance by logtransform',
                                'scanpy.pp.log1p(adata)']
                },
                {
                    'name' : 'Regress out unwanted variation',
                    'type' : 'code',
                    'snippet' : ['# Regress out unwanted variation',
                                 'scanpy.pp.regress_out(adata, key=)']
                },
                {
                    'name': 'Find highly variable genes',
                    'type': 'code',
                    'snippet': ['# Annotate highly variable genes within batches',
                                'scanpy.pp.highly_variable_genes(adata, batch_key= , min_mean = , min_disp = )',
                                '',
                                '# Plot mean-dispersion relationship and highly variable genes',
                                'scanpy.pl.highly_variable_genes(adata)']
                }
            ]
        },
        {
            'name' : 'Low-dimensional embedding',
            'sub-menu': [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#embeddings'
                },
                '---',
                {
                    'name': 'Perform principal component analysis (PCA)',
                    'type': 'code',
                    'snippet': ['# Embed cells into a linear subspace by PCA',
                                'scanpy.tl.pca(adata, svd_solver=\'arpack\', n_comps=100)',
                                '',
                                '# Plot PCA variance ratio for selection of PCs',
                                'scanpy.pl.pca_variance_ratio(adata, n_pcs = 30)']
                },
                {
                    'name': 'Perform Uniform Manifold Approximation and Projection (UMAP)',
                    'type': 'code',
                    'snippet': ['# Embed cells into a subspace by UMAP',
                                'scanpy.tl.umap(adata, min_dist=0.1, spread=2)']
                },
                {
                    'name' : 'Perform t-distributed stochastic neighborhood embedding (tSNE)',
                    'type': 'code',
                    'snippet' : ['# Embed cells into a subspac by tSNE',
                                 'scanpy.tl.tsne(adata, perplexity=30)']
                },
                {
                    'name' : 'Perform Diffusion Map embedding (DM)',
                    'type': 'code',
                    'snippet': ['# Embed cells into a subspace by diffusion maps',
                                'scanpy.tl.diffmap(adata, n_comps=30)']
                }
            ]
        },
        {
            'name': 'Cluster cells in subgroups',
            'sub-menu': [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#clustering-and-trajectory-inference'
                },
                '---',
                {
                    'name': 'Leiden clustering',
                    'type': 'code',
                    'snippet': [
                        '# Initial Leiden clustering into coarse subgroups',
                        'scanpy.tl.leiden(adata, resolution=0.5, key_added=\'leiden_coarse\')',
                        '',
                        '# Rename clusters, starting with 1',
                        'adata.obs[\'leiden_coarse\'].cat.categories = [str(i+1) for i in range(len(adata.obs[\'leiden_coarse\'].cat.categories))]'
                    ]
                },
                {
                    'name': 'Louvain clustering',
                    'type': 'code',
                    'snippet': [
                        '# Initial Louvain clustering into coarse subgroups',
                        'scanpy.tl.louvain(adata, resolution=0.5, key_added=\'louvain_coarse\')',
                        '',
                        '# Rename clusters, starting with 1',
                        'adata.obs[\'louvain_coarse\'].cat.categories = [str(i+1) for i in range(len(adata.obs[\'louvain_coarse\'].cat.categories))]'
                    ]
                },
            ]
        },
        {
            'name': 'Marker gene analysis',
            'sub-menu': [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#marker-genes'
                },
                '---',
                {
                    'name': 'Rank genes for characterization of groups',
                    'type': 'code',
                    'snippet': [
                        '# Rank genes between groups of cells',
                        'scanpy.tl.rank_genes_groups(adata, groupby = \'\', method=\'t-test_overestim_var\', n_genes = 250)',
                        '',
                        '# Calculate dendrogram between groups',
                        'scanpy.tl.dendrogram(adata, groupby=\'\')'
                    ]
                },
                {
                    'name': 'Visualize ranked genes in a dotplot',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.rank_genes_groups_dotplot(adata, n_genes=10, groupby=\'\')'
                    ]
                },
                {
                    'name': 'Visualize ranked genes in a matrixplot',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.rank_genes_groups_matrixplot(adata, n_genes=10, groupby=\'\', standard_scale=\'var\')'
                    ]
                },
                {
                    'name': 'Visualize marker genes per condition',
                    'type': 'code',
                    'snippet': [
                        'import warnings',
                        'import matplotlib.colors as clr',
                        'import os',
                        '',
                        '#Variabels',
                        'maximum = 0 #maxium of gene expression in umap plot ',
                        'cluster = \'\' #obs key used for overview',
                        'genes = [] #genes to plot',
                        'plot_data = adata #use \u201Cadata\u201D for all cells or create subset adata1 = adata[adata.obs[\'leiden\'].isin([\'1\'])]',
                        'title = \'\' # none for no title',
                        'conditions = \'\' #obs row by which the cells should be split',
                        '',
                        'save = \'True\' #should output been saved, True for saving',
                        'prefix = \'\'#file name will be gene name and prefix',
                        'figpath = \'\' #Path to save object',
                        '#check if genes present in object',
                        'gene_list = []',
                        'for i in genes:',
                        '    if i in adata.var.index:',
                        '        gene_list.append(i)',
                        '    else:',
                        '        warnings.warn(\'Gene {} not present in adata Object\'.format(i))',
                        '',
                        '#Create subset per condition and list of condition names',
                        'number_of_Conditons = len(set(adata.obs[conditions]))',
                        'sub_adata_per_condition = []',
                        'condition_list= []',
                        'for i in set(adata.obs[conditions]): ',
                        '    sub_adata_per_condition.append(adata[adata.obs[conditions].isin([i])])',
                        '    condition_list.append(i)',
                        '',
                        '#change format',
                        '',
                        'cmap = clr.LinearSegmentedColormap.from_list(\'custom umap\', [\'#f2f2f2\',\'#ff4500\'], N=256)',
                        'scanpy.set_figure_params(dpi=160)',
                        '',
                        'for gene in genes:',
                        '    #create axs object',
                        '    fig, axs = plt.subplots(number_of_Conditons,2,figsize=(10,18),sharey=\'all\',sharex=\'all\',constrained_layout=True)',
                        '    if title != \'none\':',
                        '        fig.suptitle(title,fontweight=\'bold\')',
                        '    #Calculate Max value for Gene',
                        '    for l in range(0,number_of_Conditons):',
                        '        #caluclate score of gene to get value for y vmax',
                        '        scanpy.tl.score_genes(sub_adata_per_condition[l],[gene])',
                        '        #if a gene has a higher expression then max max is set ot gene value',
                        '        a = max(sub_adata_per_condition[l].obs[\'score\'])',
                        '        if a > maximum:',
                        '            maximum = a',
                        '        del sub_adata_per_condition[l].obs[\'score\'] ',
                        '    #Plot',
                        '    for l in range(0,number_of_Conditons):',
                        '        sc.pl.umap(sub_adata_per_condition[l], vmax = maximum,color=[gene], color_map = cmap , alpha_img=0.7, show=False, ax=axs[l,1])',
                        '        sc.pl.umap(sub_adata_per_condition[l], color=[cluster],legend_loc=\'on data\',show=False, ax=axs[l,0])',
                        '        axs[l][0].set_title(condition_list[l],fontweight=\'bold\')',
                        '        axs[l][1].set_title(gene,fontweight=\'bold\')',
                        '    #Save  ',
                        '    if save:',
                        '        if not os.path.exists(figpath):',
                        '            os.makedirs(figpath)',
                        '        plt.savefig(str(figpath)+str(prefix)+str(gene)+\'.pdf\')',
                    ]
                },
            ]
        },
        {
            'name': 'Visualizations',
            'sub-menu': [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/scanpy.plotting.html'
                },
                '---',
                {
                    'name': 'Scatter plot in PCA coordinates',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.pca(adata, color = [], title = [], legend_loc = \'right margin\')'
                    ]
                },
                {
                    'name': 'Scatter plot cells in UMAP basis',
                    'type': 'code',
                    'snippet': [
                        '# Visualize cells in UMAP',
                        'scanpy.pl.umap(adata, color = [], title = [], legend_loc = \'right margin\')'
                    ]
                },
                {
                    'name': 'Matrix plot of mean gene expression per cluster',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.matrixplot(adata, var_names = [], groupby=None)'
                    ]
                },
                {
                    'name': 'Stacked violin plot',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.stacked_violin(adata, var_names = [], groupby=None)'
                    ]
                },
                {
                    'name': 'Violin plot',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.violin(adata, keys = [], groupby = None, rotation=90)'
                    ]
                },
                {
                    'name': 'Dot plot of mean expression and percentage of cells',
                    'type': 'code',
                    'snippet': [
                        'scanpy.pl.dotplot(adata, var_names = [], groupby=None)'
                    ]
                },
            ]
        },
        {
            'name': 'Receptor-Ligand-Interaction',
            'sub-menu': [
                {
                    'name': 'Setup',
                    'type': 'code',
                    'snippet': [
                        'import pandas as pd',
                        'from collections import Counter',
                        'import scipy',
                        'import numpy as np',
                        'import matplotlib.pyplot as plt',
                        'import seaborn as sns',
                        'from pathlib import Path',
                        'import os',
                        'import igraph as ig',
                        'from itertools import combinations_with_replacement',
                        'from matplotlib import cm'
                    ]
                },
                {
                    'name': 'Download cellphone interaction database',
                    'type': 'code',
                    'snippet': [
                        '# cellphone database (human)',
                        'interaction_db = pd.read_csv("https://raw.githubusercontent.com/Teichlab/cellphonedb-data/master/data/interaction_input.csv")',
                        '# drop interactions when there are no protein names',
                        'interaction_db.dropna(subset=["protein_name_a", "protein_name_b"], inplace=True)',
                        '# remove \'_human\' suffixes',
                        'interaction_db["protein_name_a"] = [prot.split("_")[0] for prot in interaction_db["protein_name_a"]]',
                        'interaction_db["protein_name_b"] = [prot.split("_")[0] for prot in interaction_db["protein_name_b"]]'
                    ]
                },
                '---',
                {
                    'name': 'Compute interaction table',
                    'type': 'code',
                    'snippet': [
                        'def calculate_interaction_table(adata, cluster_column, partner_a, partner_b, custom_index):',
                        '    \'\'\'',
                        '    Calculate a interaction table of the clusters defined in adata.',
                        '    ',
                        '    Parameters:',
                        '        adata (adata): Adata object that holds the expression values and clustering',
                        '        cluster_column (str): Name of the cluster column in adata.obs',
                        '        partner_a (list): Names of the first interaction partners. Corresponds to partner_b.',
                        '        partner_b (list): Names of the second interaction partners. Corresponds to partner_a.',
                        '        custom_index (str): Which column to use as adata.var index. If None will use the index.',
                        '        ',
                        '    Returns:',
                        '        interactions (DataFrame): Returns pandas DataFrame of all interactions between clusters.',
                        '    \'\'\'',
                        '    ',
                        '    ########## compute cluster means and expression percentage for each gene ##########',
                        '    cluster_mean_cols = []',
                        '    perc_cols = []',
                        '    ',
                        '    for cluster in set(adata.obs[cluster_column]):',
                        '        print(f"Calculating {cluster} scores")',
                        '        ',
                        '        cluster_data = adata[adata.obs[cluster_column] == cluster].copy()',
                        '        ',
                        '        # compute cluster means',
                        '        cluster_mean_cols.append(f"{cluster}_cluster_means")',
                        '        # TODO use median',
                        '        if custom_index is None:',
                        '            adata.var.loc[adata.var.index.isin(cluster_data.var.index), cluster_mean_cols[-1]] = cluster_data.X.mean(axis=0).reshape(-1,1)',
                        '        else:',
                        '            adata.var.loc[adata.var[custom_index].isin(cluster_data.var[custom_index]), cluster_mean_cols[-1]] = cluster_data.X.mean(axis=0).reshape(-1,1)',
                        '    ',
                        '        # compute expression percentage',
                        '        perc_cols.append(f"{cluster}_cluster_percentage")',
                        '        rows, cols = cluster_data.X.nonzero()',
                        '        gene_occurence = Counter(cols)',
                        '        adata.var[perc_cols[-1]] = 0',
                        '        adata.var.iloc[list(gene_occurence.keys()), adata.var.columns.get_loc(perc_cols[-1])] = list(gene_occurence.values())',
                        '        adata.var[perc_cols[-1]] = adata.var[perc_cols[-1]] / len(cluster_data.obs)',
                        '    ',
                        '    # aggregate means/ percentage for mouse genes that mapped to the same human gene',
                        '    cluster_means = adata.var[cluster_mean_cols].groupby(adata.var.index).mean()',
                        '    cluster_perc = adata.var[perc_cols].groupby(adata.var.index).mean()',
                        '    ',
                        '    ########## compute zscore of cluster means for each gene ##########',
                        '    zscores = cluster_means.apply(lambda x: pd.Series(scipy.stats.zscore(x, nan_policy=\'omit\'), index=cluster_means.columns), axis=1)',
                        '    ',
                        '    interactions = {"cluster_a": [],',
                        '                "cluster_b": [],',
                        '                "partner_a": [],',
                        '                "partner_b": [],',
                        '                "score_a": [],',
                        '                "score_b": [],',
                        '                "percentage_a": [],',
                        '                "percentage_b": []}',
                        '    ',
                        '    ########## create interaction table ##########',
                        '    for prot_a, prot_b in zip(partner_a, partner_b):',
                        '        if prot_a is np.nan or prot_b is np.nan:',
                        '            continue',
                        '    ',
                        '        if not prot_a in zscores.index or not prot_b in zscores.index:',
                        '            continue',
                        '    ',
                        '        for cluster_a, perc_a in zip(zscores.columns, cluster_perc.columns):',
                        '            for cluster_b, perc_b in zip(zscores.columns, cluster_perc.columns):',
                        '                interactions["partner_a"].append(prot_a)',
                        '                interactions["partner_b"].append(prot_b)',
                        '                interactions["cluster_a"].append(cluster_a.split("_cluster_means")[0])',
                        '                interactions["cluster_b"].append(cluster_b.split("_cluster_means")[0])',
                        '                interactions["score_a"].append(zscores.loc[prot_a, cluster_a])',
                        '                interactions["score_b"].append(zscores.loc[prot_b, cluster_b])',
                        '                interactions["percentage_a"].append(cluster_perc.loc[prot_a, perc_a])',
                        '                interactions["percentage_b"].append(cluster_perc.loc[prot_b, perc_b])',
                        '    ',
                        '    interactions = pd.DataFrame(interactions)',
                        '    interactions["interaction_score"] = interactions["score_a"] + interactions["score_b"]',
                        '    ',
                        '    return interactions'
                    ]
                },
                {
                    'name': 'Interaction violin plot',
                    'type': 'code',
                    'snippet': [
                        'def interaction_violin_plot(interactions, min_perc, output, figsize=(14,20)):',
                        '    \'\'\'',
                        '    Generate violin plot of pairwise cluster interactions.',
                        '    ',
                        '    Parameters:',
                        '        interactions (DataFrame): Interactions table as given by calculate_interaction_table().',
                        '        min_perc (float): Minimum percentage of cells in a cluster each gene must be expressed in.',
                        '        output (str): Path to output file.',
                        '        figsize (int tuple): Tuple of plot (width, height).',
                        '    \'\'\'',
                        '    ',
                        '    rows = len(set(interactions["cluster_a"]))',
                        '',
                        '    fig, axs = plt.subplots(ncols=1, nrows=rows, figsize=figsize, tight_layout={\'rect\': (0, 0, 1, 0.95)}) # prevent label clipping; leave space for title',
                        '    #fig.suptitle(\'Cluster interactions\', fontsize=16)',
                        '    flat_axs = axs.flatten()',
                        '',
                        '    # generate violins of one cluster vs rest in each iteration',
                        '    for i, cluster in enumerate(sorted(set(interactions["cluster_a"].tolist() + interactions["cluster_b"].tolist()))):',
                        '        cluster_interactions = interactions[((interactions["cluster_a"] == cluster) | ',
                        '                                            (interactions["cluster_b"] == cluster)) &',
                        '                                            (interactions["percentage_a"] >= min_perc) &',
                        '                                            (interactions["percentage_b"] >= min_perc)].copy()    ',
                        '        cluster_interactions["Cluster"] = cluster_interactions.apply(lambda x: x[1] if x[0] == cluster else x[0], axis=1).tolist()',
                        '',
                        '        plot = sns.violinplot(x=cluster_interactions["Cluster"],',
                        '                    y=cluster_interactions["interaction_score"], ',
                        '                    ax=flat_axs[i])',
                        '        ',
                        '        plot.set_xticklabels(plot.get_xticklabels(), rotation=90)',
                        '        ',
                        '        flat_axs[i].set_title(f"Cluster {cluster}")',
                        '',
                        '    # create path if necessary',
                        '    Path(os.path.dirname(output)).mkdir(parents=True, exist_ok=True)',
                        '    fig.savefig(output)'
                    ]
                },
                {
                    'name': 'Hairball network plot',
                    'type': 'code',
                    'snippet': [
                        'def hairball(interactions, min_perc, interaction_score, output, title, color_min=None, color_max=None):',
                        '    \'\'\'',
                        '    Generate network graph of interactions between clusters.',
                        '    ',
                        '    Parameters:',
                        '        interactions (DataFrame): Interactions table as given by calculate_interaction_table().',
                        '        min_perc (float): Minimum percentage of cells in a cluster each gene must be expressed in.',
                        '        interaction_score (float): Interaction score must be above this threshold for the interaction to be counted in the graph.',
                        '        output (str): Path to output file.',
                        '        title (str): The plots title.',
                        '        color_min (float): Min value for color range. If None = 0',
                        '        color_max (float): Max value for color range. If None = np.max',
                        '    \'\'\'',
                        '    igraph_scale=3',
                        '    matplotlib_scale=4',
                        '    ',
                        '    ########## setup class that combines igraph with matplotlib ##########',
                        '    # from https://stackoverflow.com/a/36154077',
                        '    # makes igraph compatible with matplotlib',
                        '    # so a colorbar can be added',
                        '',
                        '    from matplotlib.artist import Artist',
                        '    from igraph import BoundingBox, Graph, palettes',
                        '',
                        '    class GraphArtist(Artist):',
                        '        """Matplotlib artist class that draws igraph graphs.',
                        '',
                        '        Only Cairo-based backends are supported.',
                        '        """',
                        '',
                        '        def __init__(self, graph, bbox, palette=None, *args, **kwds):',
                        '            """Constructs a graph artist that draws the given graph within',
                        '            the given bounding box.',
                        '',
                        '            `graph` must be an instance of `igraph.Graph`.',
                        '            `bbox` must either be an instance of `igraph.drawing.BoundingBox`',
                        '            or a 4-tuple (`left`, `top`, `width`, `height`). The tuple',
                        '            will be passed on to the constructor of `BoundingBox`.',
                        '            `palette` is an igraph palette that is used to transform',
                        '            numeric color IDs to RGB values. If `None`, a default grayscale',
                        '            palette is used from igraph.',
                        '',
                        '            All the remaining positional and keyword arguments are passed',
                        '            on intact to `igraph.Graph.__plot__`.',
                        '            """',
                        '            Artist.__init__(self)',
                        '',
                        '            if not isinstance(graph, Graph):',
                        '                raise TypeError("expected igraph.Graph, got %r" % type(graph))',
                        '',
                        '            self.graph = graph',
                        '            self.palette = palette or palettes["gray"]',
                        '            self.bbox = BoundingBox(bbox)',
                        '            self.args = args',
                        '            self.kwds = kwds',
                        '',
                        '        def draw(self, renderer):',
                        '            from matplotlib.backends.backend_cairo import RendererCairo',
                        '            if not isinstance(renderer, RendererCairo):',
                        '                raise TypeError("graph plotting is supported only on Cairo backends")',
                        '            self.graph.__plot__(renderer.gc.ctx, self.bbox, self.palette, *self.args, **self.kwds)',
                        '    ',
                        '    ########## create igraph ##########',
                        '    graph = ig.Graph()',
                        '',
                        '    # set nodes',
                        '    clusters = list(set(list(interactions["cluster_a"]) + list(interactions["cluster_b"])))',
                        '    ',
                        '    graph.add_vertices(clusters)',
                        '    graph.vs[\'label\'] = clusters',
                        '    graph.vs[\'size\'] = 45',
                        '    graph.vs[\'label_size\'] = 30',
                        '',
                        '    # set edges',
                        '    for (a, b) in combinations_with_replacement(clusters, 2):',
                        '        subset = interactions[(((interactions["cluster_a"] == a) & (interactions["cluster_b"] == b)) |',
                        '                            ((interactions["cluster_a"] == b) & (interactions["cluster_b"] == a))) &',
                        '                            (interactions["percentage_a"] >= min_perc) &',
                        '                            (interactions["percentage_b"] >= min_perc) &',
                        '                            (interactions["interaction_score"] > interaction_score)]',
                        '',
                        '        graph.add_edge(a, b, weight=len(subset))#, label=len(subset)) # add label to show edge labels',
                        '',
                        '    # set edge colors/ width based on weight',
                        '    colormap = cm.get_cmap(\'viridis\', len(graph.es))',
                        '    print(f"Max weight {np.max(np.array(graph.es[\'weight\']))}")',
                        '    max_weight = np.max(np.array(graph.es[\'weight\'])) if color_max is None else color_max',
                        '    for e in graph.es:',
                        '        e["color"] = colormap(e["weight"] / max_weight, e["weight"] / max_weight)',
                        '        e["width"] = (e["weight"] / max_weight) * 10',
                        '        ',
                        '    ########## setup matplotlib plot and combine with igraph ##########',
                        '    # Make Matplotlib use a Cairo backend',
                        '    import matplotlib',
                        '    matplotlib.use("cairo")',
                        '',
                        '    # Create the figure',
                        '    fig, axes = plt.subplots(1, 2, figsize=(8*matplotlib_scale, 6*matplotlib_scale), gridspec_kw={\'width_ratios\': [20, 1]})',
                        '    fig.suptitle(title, fontsize=12*matplotlib_scale)',
                        '',
                        '    # hide axis under network graph',
                        '    axes[0].axis(\'off\')',
                        '',
                        '    # Draw the graph over the plot',
                        '    # Two points to note here:',
                        '    # 1) we add the graph to the axes, not to the figure. This is because',
                        '    #    the axes are always drawn on top of everything in a matplotlib',
                        '    #    figure, and we want the graph to be on top of the axes.',
                        '    # 2) we set the z-order of the graph to infinity to ensure that it is',
                        '    #    drawn above all the curves drawn by the axes object itself.',
                        '    left = 250',
                        '    top = 150',
                        '    width = 500 * igraph_scale + left',
                        '    height = 500 * igraph_scale + top',
                        '    graph_artist = GraphArtist(graph, (left, top, width, height), layout=graph.layout_circle(order=sorted(clusters)))',
                        '    graph_artist.set_zorder(float(\'inf\'))',
                        '    axes[0].artists.append(graph_artist)',
                        '',
                        '    # add colorbar',
                        '    cb = matplotlib.colorbar.ColorbarBase(axes[1], ',
                        '                                        orientation=\'vertical\', ',
                        '                                        cmap=colormap,',
                        '                                        #label=\'Receptor-Ligand Interactions\',',
                        '                                        norm=matplotlib.colors.Normalize(0 if color_min is None else color_min, ',
                        '                                                                        max_weight))',
                        '    ',
                        '    cb.ax.tick_params(labelsize=10*matplotlib_scale) ',
                        '',
                        '    # prevent label clipping out of picture',
                        '    plt.tight_layout()',
                        '',
                        '    # create path if necessary',
                        '    Path(os.path.dirname(output)).mkdir(parents=True, exist_ok=True)',
                        '    ',
                        '    # Save the figure',
                        '    fig.savefig(output)'
                    ]
                },
                {
                    'name': 'Progress violin plot',
                    'type': 'code',
                    'snippet': [
                        'def progress_violins(datalist, datalabel, cluster_a, cluster_b, min_perc, output, figsize=(12, 6)):',
                        '    \'\'\'',
                        '    Show cluster interactions over timepoints.',
                        '    ',
                        '    Parameters:',
                        '        datalist (list): List of interaction DataFrames. Each DataFrame represents a timepoint.',
                        '        datalabel (list): List of strings. Used to label the violins.',
                        '        cluster_a (str): Name of the first interacting cluster.',
                        '        cluster_b (str): Name of the second interacting cluster.',
                        '        min_perc (float): Minimum percentage of cells in a cluster each gene must be expressed in.',
                        '        output (str): Path to output file.',
                        '        figsize (int tuple): Tuple of plot (width, height).',
                        '    \'\'\'',
                        '    ',
                        '    fig, axs = plt.subplots(1, len(datalist), figsize=figsize)',
                        '    fig.suptitle(f"{cluster_a} - {cluster_b}")',
                        '    ',
                        '    flat_axs = axs.flatten()',
                        '    for i, (table, label) in enumerate(zip(datalist, datalabel)):',
                        '        # filter data',
                        '        subset = table[((table["cluster_a"] == cluster_a) & (table["cluster_b"] == cluster_b) |',
                        '                        (table["cluster_a"] == cluster_b) & (table["cluster_b"] == cluster_a)) &',
                        '                    (table["percentage_a"] >= min_perc) &',
                        '                    (table["percentage_b"] >= min_perc)]',
                        '        ',
                        '        v = sns.violinplot(data=subset, y="interaction_score", ax=flat_axs[i])',
                        '        v.set_xticklabels([label])',
                        '        ',
                        '    plt.tight_layout()',
                        '    ',
                        '    if not output is None:',
                        '        fig.savefig(output)'
                    ]
                },
                '---',
                {
                    'name': 'Interaction table call',
                    'type': 'code',
                    'snippet': [
                        'interactions = calculate_interaction_table(adata=adata,',
                        '                                           cluster_column=,',
                        '                                           partner_a=interaction_db["protein_name_a"],',
                        '                                           partner_b=interaction_db["protein_name_b"]],',
                        '                                           custom_index=None)'
                    ]
                },
                {
                    'name': 'Interaction violin call',
                    'type': 'code',
                    'snippet': [
                        'rows = len(set(interactions["cluster_a"].tolist() + interactions["cluster_b"].tolist()))',
                        '',
                        'interaction_violin_plot(interactions=interactions, ',
                        '                        min_perc=,',
                        '                        output=,',
                        '                        figsize=(15, rows*5))',
                    ]
                },
                {
                    'name': 'Hairball network call',
                    'type': 'code',
                    'snippet': [
                        'hairball(interactions=interactions, ',
                        '         min_perc=,',
                        '         interaction_score=,',
                        '         output=,',
                        '         title=,',
                        '         color_min=None,',
                        '         color_max=None)',
                    ]
                },
                {
                    'name': 'Progress violin call',
                    'type': 'code',
                    'snippet': [
                        'progress_violins(datalist=[],',
                        '                 datalabel=[],',
                        '                 cluster_a=,',
                        '                 cluster_b=,',
                        '                 min_perc=,',
                        '                 output=None)',
                    ]
                }
            ]
        }
    ],
});
