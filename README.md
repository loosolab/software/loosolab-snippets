# Loosolab snippets

Adds a menu item to Jupyter notebooks to insert snippets and documentation that is often used in the Loosolab.

## Installation

```
git clone https://gitlab.gwdg.de/loosolab/software/loosolab-snippets.git
jupyter nbextension install loosolab-snippets --user
jupyter nbextension enable loosolab-snippets/main --user
```

# Add new snippet

To create a new snippet submenu, you have to add the file's config.yaml and main.js and create a new file snippets_submenu_<name>.js.

## Create snippet file

First, create a new sub menu's file like the following:

```javascript
define({
    'name' : 'bbknn', //name of sub menue
    'sub-menu' : [
        {
            'name' : 'Batch balanced KNN', // name of snippet
            'type': 'code',
            'snippet' : [ //snippet code each line of code as a string in java script.
                'import bbknn',
                '',
                '# KNN procedure to identify each cell’s top neighbours in each batch separately',
                'bbknn.bbknn(adata=adata, batch_key = \'\', trim = , n_pcs = )'
            ],
        }
    ],
});
```

## Edit config.yml
Add a block like the following one for the new snippet. Edit name and description for the new snippet

```javascript
- name: snippets.include_submenu.bbknn
  description: "Include bbknn sub-menu"
  input_type: checkbox
  default: true
```

## Edit main.js

First add the name of the new snippets_submenu_<name>.js to the main.js file. line (5-11) and add the function name in line 12 :

```javascript
define([
    'require',
    'jquery',
    'base/js/namespace',
    './snippets_submenu_markdown',
    './snippets_submenu_scanpy',
    './snippets_submenu_scvelo',
    './snippets_submenu_bbknn',
    './snippets_submenu_hashsolo',
    './snippets_submenu_MaMPOK',
    './snippets_submenu_excel',
], function (require, $, Jupyter, markdown, scanpy, scvelo, bbknn, hashsolo, mampok, excel) {
```
Then add the function name to the dictionary line (18-16):

```javascript
    var menues = {
        'markdown': markdown,
        'scanpy': scanpy,
        'scvelo': scvelo,
        'bbknn': bbknn,
        'hashsolo': hashsolo,
        'mampok': mampok,
        'excel': excel
    }
```

Add the function name to the list in the default_menus dictionary key 'sub-menu' line(28-33):

```javascript
    var default_menus = [
        {
            'name' : 'Loosolab',
            'sub-menu-direction' : 'left',
            'sub-menu' : [markdown, scanpy, scvelo, bbknn, hashsolo, mampok, excel],
        },
```
As last, add the function name to the list includable_submenu_keys line (44 - 53):

```javascript
    var includable_submenu_keys = [
        "scanpy",
        "scvelo",
        "bbknn",
        "hashsolo",
        "mampok",
        "markdown",
        'excel'
        
    ];
```

# Add new submenu

To include the new sub menu to your notebooks, run:

```
jupyter nbextension install loosolab-snippets --user
jupyter nbextension enable loosolab-snippets/main --user
```
