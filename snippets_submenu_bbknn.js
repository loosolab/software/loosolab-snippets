define({
    'name' : 'bbknn',
    'sub-menu' : [
        {
            'name' : 'Documentation',
            'external-link' : 'https://bbknn.readthedocs.io/en/latest/',
        },
        {
            'name': 'Add watermark',
            'type': 'code',
            'snippet': [
                        '%load_ext watermark',
                        '%watermark -nuvhm -p numpy,pandas,anndata,scanpy,bbknn,geosketch,scvelo -a \'YOUR AUTHOR NAME\']'
            ]
        },
        '---',
        {
            'name' : 'Batch balanced KNN',
            'type': 'code',
            'snippet' : [
                'import bbknn',
                '',
                '# KNN procedure to identify each cell’s top neighbours in each batch separately',
                'bbknn.bbknn(adata=adata, batch_key = \'\', trim = , n_pcs = )'
            ],
        }
    ],
});
