define({
    'name' : 'scVelo',
    'sub-menu' : [
        {
            'name' : 'Documentation',
            'external-link' : 'https://scvelo.readthedocs.io/',
        },
        {
            'name': 'Add watermark',
            'type': 'code',
            'snippet': [
                        '%load_ext watermark',
                        '%watermark -nuvhm -p numpy,pandas,anndata,scanpy,bbknn,geosketch,scvelo -a \'YOUR AUTHOR NAME\']'
            ]
        },
        {
            'name' : 'Setup',
            'type': 'code',
            'snippet' : [
                'import numpy as np',
                'import pandas as pd',
                'import scanpy',
                'import scvelo',
                'scanpy.settings.verbosity = 3',
                'scanpy.set_figure_params(color_map=\'viridis\', dpi_save=300)',
                'scanpy.logging.print_header()'
            ],
        },
        '---',
        {
            'name': 'Load data',
            'sub-menu': [
                {
                    'name': 'Read Velocyto output from STARsolo',
                    'type': 'code',
                    'snippet': [
                        'from scipy import sparse',
                        '',
                        '# Shape of the resulting matrix',
                        'shape = ()',
                        '',
                        '# Read the samples mtx file and split into three matrices',
                        'sample = \'Sample1\'',
                        'mtx = np.loadtxt(\'../quant/{}/solo/Velocyto/raw/matrix.mtx\'.format(sample), skiprows=3, delimiter=\' \')',
                        'spliced = sparse.csc_matrix((mtx[:,2], (mtx[:,0]-1, mtx[:,1]-1)), shape = shape)',
                        'unspliced = sparse.csc_matrix((mtx[:,3], (mtx[:,0]-1, mtx[:,1]-1)), shape = shape)',
                        'ambiguous = sparse.csc_matrix((mtx[:,4], (mtx[:,0]-1, mtx[:,1]-1)), shape = shape)',
                        '',
                        '# Keep only those barcodes and features that are already in the reference object (adata) with spliced counts',
                        'barcodes = [x + \'-\' + lib for x in np.loadtxt(\'../quant/{}/solo/Velocyto/raw/barcodes.tsv\'.format(sample), dtype=str)]',
                        'keep_barcodes = [barcodes.index(bc) for bc in adata.obs_names',
                        'features = pd.read_csv(\'../quant/{}/solo/Velocyto/raw/genes.tsv\'.format(sample), sep=\'\t\', header=None, names=[\'ID\', \'Symbol\', \'Assay\']).set_index(\'Symbol\')',
                        '',
                        '# Create AnnData object for spliced, unspliced and ambigous counts',
                        'vdata = anndata.AnnData(spliced[:, keep_barcodes].T,',
                        '   layers={\'spliced\': spliced[:, keep_barcodes].T, \'unspliced\': unspliced[:, keep_barcodes].T},',
                        '   obs = adata.obs,',
                        '   var = features)'
                    ]
                },
                {
                    'name' : 'Merge two annotated data matrices',
                    'type': 'code',
                    'snippet': ['# Merge unspliced (adata) and spliced (vdata) annotated data matrices',
                                'udata = scvelo.utils.merge(adata, vdata)'],
                }
            ]
        },
        {
            'name' : 'Preprocessing',
            'sub-menu' : [
                {
                    'name' : 'Documentation',
                    'external-link' : 'https://scanpy.readthedocs.io/en/stable/api/index.html#module-scanpy.pp'
                },
                '---',
                {
                    'name': 'Show proportions of spliced and unspliced counts',
                    'type': 'code',
                    'snippet': [
                        '# Show proportions of spliced vs. unspliced',
                        'scvelo.utils.show_proportions(adata)'
                    ]
                },
                {
                    'name' : 'Filter and normalize data',
                    'type': 'code',
                    'snippet': ['scvelo.pp.filter_and_normalize(adata)']
                },
                {
                    'name' : 'Remove duplicated cells',
                    'type': 'code',
                    'snippet': ['scvelo.pp.remove_duplicate_cells(adata)']
                },
                {
                    'name' : 'Calculate moments',
                    'type' : 'code',
                    'snippet': [
                        '# Calculate moments', 
                        'scvelo.pp.moments(adata)']
                }
            ]
        },
        {
            'name' : 'Tools',
            'sub-menu': [
                {
                    'name': 'Recover full splicing dynamics',
                    'type': 'code',
                    'snippet': [
                        'scvelo.tl.recover_dynamics(adata)',
                        'scvelo.tl.velocity(udata, mode=\'dynamical\')']
                },
                {
                    'name': 'Calculate velocity graph',
                    'type': 'code',
                    'snippet': ['scvelo.tl.velocity_graph(adata)']
                }
            ]
        },
        {
            'name': 'Visualization',
            'sub-menu': [
                {
                    'name': 'Visualize on top of an embedding',
                    'type': 'code',
                    'snippet': [
                        'scvelo.pl.velocity_embedding(udata, basis=\'umap\',',
                        'color = [],',
                        'scale = 0.5, arrow_size = 1.5,',
                        'legend_loc=\'right margin\')']
                },
                {
                    'name': 'Visualize on top of an embedding as grid',
                    'type': 'code',
                    'snippet': [
                        'scvelo.pl.velocity_embedding_grid(udata, basis=\'umap\',',
                        'color = [],',
                        'scale = 0.5, arrow_size = 1.5,',
                        'legend_loc=\'right margin\')']
                },
                {
                    'name': 'Visualize as stream plot',
                    'type': 'code',
                    'snippet': [
                        'scvelo.pl.velocity_embedding_stream(udata, basis=\'umap\',',
                        'color = [],',
                        'scale = 0.5, arrow_size = 1.5,',
                        'legend_loc=\'right margin\')']
                }
            ]
        }
    ],
});
