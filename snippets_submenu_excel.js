define({
    'name' : 'Excel output',
    'sub-menu' : [
        {
            'name': 'Adata to Excel',
            'type': 'code',
            'snippet': [
                'genes_of_intress = [<genenames>]',
                'adata_sub = adata[:,adata.var.index.isin(genes_of_intress)].copy()',
                'frame = pd.DataFrame(data=adata_sub.X.toarray(),index=adata_sub.obs.index.tolist(),columns=adata_sub.var.index)',
                'frame.to_excel(<outfile>)'
            ]
        },
        {
            'name': 'High Variable Genes to Excel',
            'type': 'code',
            'snippet': [
                'def qc(adata):',
                '   is_mito = adata.var_names.str.startswith(\'mt\')',
                '   adata.var[\'is_mito\'] = is_mito',
                '   qc_metrics = scanpy.pp.calculate_qc_metrics(adata = adata, qc_vars = [\'is_mito\'], inplace = False)',
                '   adata.obs[\'n_genes_by_counts\'] = qc_metrics[0][\'n_genes_by_counts\']',
                '   adata.obs[\'log1p_total_counts\'] = qc_metrics[0][\'log1p_total_counts\']',
                '   adata.obs[\'pct_counts_is_mito\'] = qc_metrics[0][\'pct_counts_is_mito\']',
            
                '   adata.var[\'n_cells_by_counts\'] = qc_metrics[1][\'n_cells_by_counts\']',
                '   adata.var[\'log1p_mean_counts\'] = qc_metrics[1][\'log1p_mean_counts\']',
                '   adata.var[\'mean_counts\'] = qc_metrics[1][\'mean_counts\']',
                '   adata.var[\'total_counts\'] = qc_metrics[1][\'total_counts\']',
                '   adata.obs[\'n_counts\'] = adata.X.sum(axis=1).A1',
                '   return adata,qc_metrics',
                '',
                '#Extracts unique up and down regulated genes per cluster. Set rankby_abs = True to get down regulated genes.',
                '',
                'scanpy.tl.rank_genes_groups(adata, groupby=<column on which diff is calculated >, n_genes=250,  method= \'wilcoxon\', rankby_abs = True)',
                'scanpy.tl.dendrogram(adata, groupby=<column on which diff is calculated >)',
                '',
                '#Set out put of excel and obs column',
                '',
                'path = <excel out file>',
                'writer = pd.ExcelWriter(path, engine = \'xlsxwriter\')',
                'for c in adata.obs[<column on which diff is calculated >].cat.categories:',
                '    adata_sub= adata[adata.obs[<column on which diff is calculated >] == c,:]',
                '    qc(adata_sub)',
                '    d = {\'gene\' : adata.uns[\'rank_genes_groups\'][\'names\'][c], ',,
                '            \'logfc\' : adata.uns[\'rank_genes_groups\'][\'logfoldchanges\'][c],',
                '            \'pval\' : adata.uns[\'rank_genes_groups\'][\'pvals\'][c], ',
                '            \'qval\' : adata.uns[\'rank_genes_groups\'][\'pvals_adj\'][c],',
                '            \'score\': adata.uns[\'rank_genes_groups\'][\'scores\'][c]}',
                '    df = pd.DataFrame(d)',
                '    df = df[df[\'logfc\']<0]',
                '    df = df.set_index(\'gene\')',
                '    df = pd.merge(df,adata_sub.var, left_index=True, right_index=True)',
                '    df.to_excel(writer, sheet_name=\'Cluster_\' + c + \'_down\')',
                '    d = {\'gene\' : adata.uns[\'rank_genes_groups\'][\'names\'][c],' ,
                '            \'logfc\' : adata.uns[\'rank_genes_groups\'][\'logfoldchanges\'][c],',
                '            \'pval\' : adata.uns[\'rank_genes_groups\'][\'pvals\'][c],',
                '            \'qval\' : adata.uns[\'rank_genes_groups\'][\'pvals_adj\'][c],',
                '            \'score\': adata.uns[\'rank_genes_groups\'][\'scores\'][c]}',
                '    df = pd.DataFrame(d)',
                '    df = df[df[\'logfc\']>0]',
                '    df = df.set_index(\'gene\')',
                '    df = pd.merge(df,adata_sub.var, left_index=True, right_index=True)',
                '    df.to_excel(writer, sheet_name=\'Cluster_\' + c + \'_up\')',
                    
                'writer.save()',
                'writer.close()'
            ]
        }
    ],
});
