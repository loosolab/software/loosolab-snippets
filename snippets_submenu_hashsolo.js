define({
    'name' : 'Hashsolo',
    'sub-menu' : [
        {
            'name' : 'Documentation',
            'external-link' : 'https://github.com/calico/solo',
        },
        '---',
        {
            'name' : 'Doublet detection via semi-supervised deep learning',
            'type': 'code',
            'snippet' : [
                'from solo import hashsolo',
                '',
                '# Find hashtag features',
                'hashtag_features = adata.var.index[adata.var.index.str.contains(\'HashTag*\')].tolist()',
                '',
                '# Subset adata',
                'hdata = adata[:, hashtag_features].copy()',
                '',
                '# Rewrite counts layer as dense matrix https://github.com/calico/solo/issues/33',
                '# hdata.X = hdata.X.todense()',
                '',
                '# Run hashsolo',
                'hashsolo.hashsolo(hdata)',
                '',
                '# Write observation back to original adata object',
                'hashsolo_classification = hdata.obs[\'Classification\']',
                'adata.obs = adata.obs.join(hashsolo_classification)'
            ],
        }
    ],
});
